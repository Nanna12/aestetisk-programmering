let mousePush = false;
let ctracker;
let capture;
let blink=0



function setup() {
  createCanvas(600, 600);
  /*i want the camera on the computer to turn on,so the user can see the face*/
  capture=createCapture(VIDEO);
  capture.size(600,600);
  capture.hide();

}

function draw() {
  if (!mousePush){ /* this means that the mouse is not pressed, so if the mouse
    had been pressed, then the button will not be drawn. !: means logical NOT*/
background(23,32,42);
/* the making of my button*/
fill(color(255,0,0));
ellipse(300,300,400,400);

/* The text on my button*/
textSize(40);
fill(color(0));
strokeWeight(8)
text("DO NOT PRESS",150,300);

}else{

/*here i choose how big the canvas should be for the camera*/
image(capture,0,0,600,480);

/* Now i want the text to flickering*/
/* each time the code is being run, it increse with one*/
blink=blink+1
if(blink% 10==0){ /* if blink is divisible by 10, fill the text with a grey
color.*/
fill(50);
}else{
  fill(255);
}

/* text to the screen after the red bottun*/
textSize(40);
strokeWeight(8)
text("BIG BROTHER IS WATCHING!",20,350);

/* text 100% upload*/
textSize(40);
strokeWeight(8)
text("100% upload complete",20,400);
}
}

function mousePressed(){
/* when you push the button, the screen becomes white, so i get a new canvas
to draw on*/
  mousePush = true;
  /*here i choose that you can only press the button, if you are ind
  the radius of the button. I do it by telling the computer the radius of
  the button is 200. So you can press if the mouse is within the radius 200 or
  under*/
  let d=dist(mouseX,mouseY,300,300);
  if (d < 200){
    background(255);

  }
}
