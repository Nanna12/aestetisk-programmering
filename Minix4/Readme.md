# MiniX4 

## Link til koden: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix4/sketch.js

## Link til program:
https://nanna12.gitlab.io/aestetisk-programmering/Minix4/

## Provide a title for and a short description of your work (1000 characters/tegn or less) as if you were going to submit it to the festival.

The name of my artwork is “Watching you”. I want people to rise they awareness about how much data´s about yourself the government know about you. The phrase “Big Brother is Watching You” refers to the government´s surveillance of people, using listening devises and cameras. My artwork wants to raise awareness about surveillance and the theme data capture, so people will think a little more about how they act on the internet, or just what people agree to, such as cookies. The first part of my artwork is a symbolic red button, which says “Do not press”. It is a specific choice from my side, because I want to intrigue the user, such as I personally think a lot of webpages do. They trie to lure people to accept different terms, example by using cookies. The red button is something people have seen before, and I think that a lot of people have some kind of experience with it, both trying to press it or trying to control yourself, so you don´t press it. The minute they press the red button in my artwork, it should indicate that you now have giving the computer total control over your personal information’s/ data, because the user has given the computer acceptance to do it, by pressing the button. Besides that, if you weren’t being watch before, you are defiantly now. 

## Describe your program and what you have used and learnt.
The first think you see is a red button, where there is a text saying, “Do not press”. When you press the button in the button area, you are being sent to another page, where your computer camera turns on and you see yourself. In the right corner, you see a surveillance camera, to indicate that you are now being watch. At the end of the page, there is a text, saying “Big Brother is watching” and “100% upload complete”, the texts is flickering. I choose to make the text flicker, to make sure people notes the text.

In my code I used two DOM elements, the mouse capture and video/face capture. The mouse capture is being used, so that you can press the red button, and go to a new page. The video/face capture is being used as a scare element, because it should show that your computer is filming you all the time and your every move on your computer. In this week I have learnt to use DOM elements, and I have played with it. 


## Articulate how your program and thinking address the theme of “capture all.”
My artwork addresses the theme “Capture all” by rising awareness of how the computer can lure people in to giving their data, that’s why I have the text 100% upload complete and used the DOM element video/data capture. Also, how you can program something by using the computer, to lure data and information’s out of ordinary people. These thoughts is what made me create the artwork, that is my Minix4. 


## What are the cultural implications of data capture?
One of them is the way that states or corporations use data capture/datafication to discriminate different classes and ethnic populations in terms of political economy, legal stuff and so on. 
It we look at the legal stuff, there is the term GDPR, which is the protection of personal data for a person, the protection of personal data is often not taking into consideration. I therefore think that there should be ways of making sure that peoples protection of personal data and privacy keeps being private and not misused. Another thing in this area is artificial intelligence, self-tracking devices. How can we make sure that it is not being used incorrect, an example of this is the Apple airtag. The airtag can be used by stalkers, to see where the person they are stalking, is all the time. Which I find scary, I therefore hope that people will become more aware of what they agree to, when browsing the internet, or downloading stuff. 


## GIF:
![The program show´s a button](Minix4/giphy.gif "preview")

## Reference:
Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021
https://policyreview.info/concepts/datafication

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

