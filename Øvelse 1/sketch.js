
function preload(){
dataIngredients=loadJSON('ingredients.json');
dataInstructions=loadJSON('instructions.json');
/* i implement the JSON files in my sketch file*/
}

function setup(){
createCanvas(400, 100);
let instructions=dataInstructions.instructions;
let ingredients=dataIngredients.ingredients;
/* i make a variable out of the json file*/

let dry=ingredients[0].dry_ingredients;
let liquid=ingredients[1].liquid_ingredients;
let other=ingredients[2].other_ingredients;
let made=ingredients[3].made_with;
/* here i choose when the 4 diffrent ingredients shall be used*/


for(let i = 0; i < 4; i++){
let randomIngredient
if (i===0){
randomIngredient=random(dry);
}
else if (i===1){
randomIngredient=random(liquid);
}

else if (i===2){
randomIngredient=random(other);
}

else if (i===3){
randomIngredient=random(made);

/* in these if-statements and forloop, i say that when i is 0 the random
ingredients should come from the first category "dry"*/
}
createP(instructions[i].replace('{0}',randomIngredient));
/* here i make sure that the text i shown in the sketch file, because the text
is html. i say that the diffrent in structions should be replaced with a
random ingrdient*/

button = createButton("New recipe"); //Button to make a new recipe, if it is pushed
      button.position(350, 40);
      button.mousePressed(reload)
}
}
function draw(){
background(255,0,0);
textSize(50);
text("The recipe for your favorite cake",350,55);
}

function reload() { //Function used for the button, to make the program reload, if it is pushed
  location.reload();
}
