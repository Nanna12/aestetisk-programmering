let angle=0 /* I put angle in a container*/

function setup(){
  createCanvas(800,800);
  angleMode(DEGREES); /* i choose that angle should be degrees*/
background(40,116,166);
}

let lamp=0/* I put angle in a container*/
  function draw(){
    background(40,116,166);
    /*The body of the windmill is being made*/
    strokeWeight(0);
    fill(color(202,207,210));
    ellipse(250,650,70,600);
    ellipse(250,375,40,40)


/* now to the wings*/
push(); /*a word for save, it helps remeber the rotation and the origin (punkt)*/
translate(250,370);
/* Here i move the 0,0 down to the middel of the windmill.*/
strokeWeight(10)
if(lamp===255)
{
/* I want the rotation to start, when i turn on the lamp. Thats way i make a if-statement, where i tell the lamp that when it is 255
turned on, the rotation should begin..*/

angle = angle+1 /* I put angle down here because the rotation should only happen, when the lamp is turned on*/
}
rotate(angle)/* i move the rotate function out of the if-statement, because the function makes the wings rotate and angle determinates how much the wing should rotate. Meaning that angle only changes, when the lamp is turned on. It means when the lamp is not turned on, it stills rotate, men the angle which is being rotated with dosen´t change*/
line(5,0,155,130);
line(-5,0,-140,130);
line(0,0,0,-180);

/* i write (-250,-370) to make sure the 0,0 punkt is moved back to left corner*/
/*I will now tried making the wing move*/

/* This is the house, which electricity comes form the windmill energy*/
pop(); /* Another word for restore, it means to remake*/
strokeWeight(2);/* the lines size*/
rect(450,650,150,150)
fill(color(141, 153, 160));
triangle(450,650,525,550,600,650);

/* Now to the brichs, så it looks like a house nu til murstenen*/
fill(color(150, 25, 25));
rect(450,650,65,20);
rect(535,650,65,20);
rect(465,680,65,20);
rect(540,680,55,20);
rect(450,710,65,20);
rect(535,710,65,20);
rect(465,740,65,20);
rect(540,740,55,20);
rect(450,770,65,20);
rect(535,770,65,20);

/* Now i will make a window, where the lamp should be inside*/
fill(color(251, 252, 252));
rect(485,690,80,80);

/* I will tried to make a lamp, that will give a light*/
fill(color(204, 209, 209));
rect(515,690,20,30);
fill(lamp,lamp,0);
ellipse(525,720,30,30);


}
/* Here i choose that if you press the lamp and it will turned on. It the value is 0, i set the value to be 255 and then the lamp will turned on. So if the lamp is turned on, which means that the value is 255, then i put the vallue to 0, which means that the lamp is not turned on.*/
function mousePressed(){
  if(lamp===0){
  lamp=255;
}else{
  lamp=0
}
}
