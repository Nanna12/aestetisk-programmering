# Mini x2 Readme

## Link til koderne:
### Vindmøllen: https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/MiniX2/sketch.js
### Solcellen: https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/MiniX2/sketch1.js

## Link til program:
### Vindmøllen: https://nanna12.gitlab.io/aestetisk-programmering/MiniX2/
### Solcellen: https://nanna12.gitlab.io/aestetisk-programmering/MiniX2/index1.html

## What did i make and what new did I learn

So i spend a lot of time thinking about the theme of my emojis. I had to put my emojis into a wider social and cultural context. I came around a lot of options, such as menstruation, truism and environ-ment. I choose environment and green energy. My first emoji is called the windmill energy, it shows a house which light/power comes from the windmill´s wind power. When you press the lamp in the house, it turns yellow and the windmill´s wings starts producing the green energy. My second emoji is called the solar panel energy. Again, it shows a house, with a lightning bulb. When you press the lightning bulb, the ellipse change, from a moon to a sun. This is to indicate that the solar panel´s gives green energy to the house. In this MiniX I tried working a bit more with variable geometry, more pre-cise “if statements”. In both my emojis i used the ”if statement”, to control the lightning bulb, the sun/moon and the windmill´s wings. It was very fun to work with again. In my first MiniX I used “If statement” very little to make the green frog moving, but my understanding was at a low level. Now I think I have a better understanding for variable geometry, because I have used it a bit more this time. So I have learnt to use variable geometry a bit more in the creation of my MiniX´s.

## Wider social and cultural context

I choose to focus on environment and green energy. I looked through the emojis on my phone and just wasn´t able to find any environment/green energy emojis. So, I was pretty sure I had to make one, not only because there wasn´t an emoji, but also because of the focus on environment that has been huge for many years now. I want my emojis to become a way of showing for the individual person that you care about the environment and you have a daily focus, or you just give the environment some throughs once in a while. I think my emojis would be perfect as a way of expressing your statement on social medias, properly with the headline “I care about our environment and green energy should play a bigger role in our society”. In the making of my emojis I didn't quite think that much about discrimination, because I decided to make figures instead of face´s. After further consideration my emojis is more recognizable for first world countries, because of their wealth. The people in these countries have seen windmill´s and solar panels before, which people from third world countries may not have seen. I therefore believe the use of my emojis will be limited to the first world countries and some countries from the second world. I also believe that the lack of knowledge about green energy is because of the difference in the cultures around the world, but also about the political and the state of economy the country has.
So overall I think my two emojis take a stand and helps create a universal problem about the knowledge of green energy and also the use of green energy. If more population groups from differ-ent countries has the economy to change their production to become greener, it would also raise awareness of different kinds of ways to use green energy and what machines, such as a windmill or solar panel. I kind of think that I had an underlined mission by creating these two emojis, I want the world to become greener, which is actually something I think about, not on a daily based, but some-times. Luckily in Denmark, we have the economy and awareness to take action, but sadly that’s not the reason in many other countries.            

## Vindmølle´s Gif:
![The program show´s a windmill](MiniX2/giphy.gif "preview")


## Solcellen´s GIF:
![The program show´s a solar panel](MiniX2/giphy-1.gif "preview")

## Reference:
Video from "The coding train" 9.1 Transformation. 
https://www.youtube.com/watch?v=o9sgjuh-CBM&t=1s

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

