let frugtplukkerSize = {
w:270,
h:150
}
let frugtplukker;
let frugtplukkerPosX;
let mini_width;
let min_apple=1;
let apple=[];
let score =0, lose=0;
let catchSound;
let gameOverSound;
let appleLostSound;
let backMusicSound;

function preload(){
frugtplukker = loadImage("frugtplukker.png");
catchSound = loadSound("catch.mp3");
gameOverSound = loadSound("gameover.2.mp3");
appleLostSound = loadSound("applelost.mp3");
backMusicSound = loadSound("backmusic.mp3");
}

function setup(){
createCanvas(windowWidth, windowHeight);
backMusicSound.play();
frugtplukkerPosX=height/2;
mini_width = width;

}

function draw(){
  background(127,179,213);
  displayScore();
  checkAppleNum();
  showApple();
  image(frugtplukker,frugtplukkerPosX,570,frugtplukkerSize.w,frugtplukkerSize.h);
  checkCatching();
  checkResult();
}

function checkAppleNum(){
if(apple.length < min_apple){ /* if there is fewer than the minimun apples, the program
  should add one more. so i have 1 apples in the air all the time.*/
  apple.push(new Apple()); /* means you are creating a new object*/
}
}

function showApple(){
  /* This draws the apples and moves the apples downwards. This foreloop means
  that it runs ass many times as there is createt apples*/
for (let i = 0; i < apple.length; i++){ /* apple.length means number of apples
  in the array*/
apple[i].show();
apple[i].move();
}
}

function checkCatching(){
  /* setting the distance between the apples*/
for(let i = 0; i < apple.length; i++){
let d=int(
  dist(frugtplukkerPosX+50, 570+frugtplukkerSize.h,apple[i].pos.x,apple[i].pos.y));

if(d < frugtplukkerSize.w-150){
score++;
catchSound.play();
apple.splice(i,1); /* tHIs means that i am deleting the apple that has been
caught by the frugtplukker*/
} else if (apple[i].pos.y > windowHeight){ /* this means that it controls when the apple
wasn´t caught, but has reach the button*/
  lose++;
  appleLostSound.play();
  apple.splice(i,1); /*splice betyder at man sletter noget*/

  }
}
}


function displayScore(){
textSize(17);
text('You have catch ' + score + " apples", 10,20);
text('You have lost ' + lose + " apples", 10,40);
text('PRESS the ARROW key to start the music',10,60)
text('PRESS the ARROW LEFT & RIGHT key to catch the apples',10,80);
}

function checkResult() {
  if (lose > 2) { /* . The lose >2 means that
    i have to lose two apples and one more before i can lose the game*/
    gameOverSound.play();
    backMusicSound.pause();
    textSize(50);
    text("GAME OVER", width/2, height/2);
    noLoop();
  }
}


function keyPressed(){
  if(keyCode=== LEFT_ARROW){
    frugtplukkerPosX-=120
  }else if (keyCode=== RIGHT_ARROW){
    frugtplukkerPosX+=120;
}
}
