# MiniX6 

## Link til koderne: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix6/sketch.js

https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix6/Apple.js

## Link til program:
If you are using Google chrome to run my program, move the curser back and forth before playing the game to start the music.
If you are using Safari, the music starts by it self.

https://nanna12.gitlab.io/aestetisk-programmering/Minix6/


## Describe how does/do your game/game objects work?

I have made a program, where you have to catch apples, with a fruit picker. I was inspired by the Tofu game code, so I used that and made a few changes. My game object is an apple, which is made by using two ellipses and one line. I have used the syntax rotate to one of the ellipses, the leaf. I have programmed the apples to fall down from the top of the canvas to the button, they can be caught by an image of a fruit picker. The apples speed is random but in an index from 3-6 integer, meaning the speed goes back and forth between the number in-between 3-6. I have used the syntax distance to make sure that the diameter of the apple and the basket of the fruit picker fits, meaning that you can catch the apple when it hits the basket of the fruit picker in a certain diameter. The distance syntax is a feature that helps my score system, because it then registers when an apple has been caught and not caught.  
To make my program feature more like a game I took a further look at a new syntax, the audio sound. I have never used it before in my programs but thought it would be funny. I have loaded 4 sounds in my program, background music, a sound when you catch an apple, when you lose an apple and when you lose the game. 

## Describe how you program the objects and their related attributes, and the methods in your game.

As I wrote in the precious chapter, my object is an apple. The apples related attributes is for example the colors of the apple itself, the leaf and the stalk of the apple. The size is also an attribute and the position of the apple. The methods/behavior for the apple in my program is how it moves from the top to the button. 

## Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?

Object-oriented programming is about how an object can be described using both property and method in the code. The used of object in program are more organized around data, rather that functions and logic. Pseudo object is object abstraction in computering, where it is about representation. As mention above the object is described by two factors. The properties are the adjectives of the object and the methods/behavior is the verb. To give an example, an apple is red, round these are the properties. Then the apple moves from the tree to the ground that is a method/behavior. This abstraction of the object is a way of handling an objects complexity by thinking about its certain details.
It is not only in creating an object abstraction occur, it exists in different layers of computering. 

## Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

I have a hard time connecting my game into a wider cultural context, because I don´t think there is one. Maybe the fact that I have used apples, which is a fruit I think everyone in the world know helps gaining a wider cultural context to my game. It I have to think of one thing more it would be that the way the game is to be played is very basic, meaning that a lot of games is played in this way. That way it is easier to understand and play my game, because it is familiar for a lot of people. 

An example of a complex abstraction I would say it could be the relationship between JavaScript and P5js. P5js is an abstraction of JavaScript in the way that it takes some of the elements/syntax of JavaScript and makes it easier to use in an new program. 

## GIF:
![The program show´s a apple catching game](Minix6/giphy.gif "preview")

## Reference:
Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on BrightSpace\Literature)
