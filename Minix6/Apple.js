
class Apple{
  constructor()
{
this.speed = floor(random(3,6));
this.pos = new createVector(random(width-80),0); /*the width i -80, because
the apples diameter is 80, so the apple stays in the canvas. The random is
placed because the apples have to appear diffrent places on the canvas. The 0
is the height because the apple starts from the top. //vector gør at den deler
positionen op i x og y.*/
this.size = floor(random(15,35));

}

move() {
  /* this linjes means that the apple starts one side of the canvas*/
this.pos.y+=this.speed;
}

show() {
push()
translate(this.pos.x,this.pos.y);
noStroke();
fill(204, 55, 51);
ellipseMode(CENTER);
ellipse(100, 100, 80, 75);
stroke(78, 38, 0);
strokeWeight(5);
line(95, 65, 100, 40);
noStroke();
rotate(radians(-30));
fill(39, 166, 21);
ellipse(45, 95, 15, 25)
pop();
}
}
