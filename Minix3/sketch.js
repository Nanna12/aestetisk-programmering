var angle = 0;

function setup() {
  createCanvas(600, 600);
	strokeWeight(4);
  stroke(30,20);
  /* i choose the size of my canvs, i choose that the lines in my program
  have to have a width on 4. besides that i choose to remove the edge
  on the rect, i do that by adding two numbers in the stroke syntax
  */

}

function draw() {
  background(68,163,173);

  /* now i draw the stilk and leafs on my flower*/
  fill(color(87,173,68));
  ellipse(250,450,110,40);
  ellipse(345,480,110,40)
  rect(290,380,20,150);


  /* Now i want some rotation*/
  push() /* another word for saving, remembering the rotation and the origin
  punkt. i start by moving the origin to the midt of my canvas, thats why i
  devide the height and width with 2*/
  translate(width/2,height/2);

  for (var i=0; i<100; i++){
    /* i tell the for-loop that i is 0 and i has to be smallere that 100. i add
    rect everytime it runs, it add a rect in the last part of the for-loop*/
  rotate(angle);
  scale(0.95);
    /* i set the scale to 0,95, that means everytime a rect is being added, the
    rect is 5% smallere*/

/* now i want the rect to have three different color, which has to be repeated
i therefore use three if-statements. % this sign means that you devide
i this case devide numbers up in 100. In the first if-statement i have writen
2, which means that everytime the number 2 goes up in 100, one specific
color is choosen. the next if-statemet is have the number 3, which means that
everytime 3 goes up in 100, a new color for the rect i chosen.
If 2 does not go up in 100, but 3 does the color two chooses.
the last if-statement says that if none of the above is true, it chooses the
last color.
*/
		if(i%2==0) fill(142, 68, 173)
			else if(i%3==0) fill(231, 76, 60 )
			else fill(247, 220, 111);

/*here i add the rect*/
    rectMode(CENTER);
    rect(0, 0, 180, 180);
  }
  /* This is where i give my rotation a speed*/
	 angle += 0.006;
   pop() /* another word for restore*/

/* add the text Loading...*/
textSize(32);
fill(color(255,0,0));
text("Loading...",230,150);

}
