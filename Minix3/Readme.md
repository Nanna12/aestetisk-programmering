# MiniX3 

## Link til koden: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix3/sketch.js

## Link til Throbber:
https://Nanna12.gitlab.io/aestetisk-programmering/Minix3/

## What do you want to explore and/or express?

I wanted to create a colorful throbber. A throbber that still looks like a throbber but consist of other things. Firstly, I thought about creating a throbber that showed time, as I a life cycle. I thought about a human, animal or plant life cycle. My first intension was to create a throbber that showed an flowers life cycle from it was a seed to a flower that is dead. I tried making it happen, by I had to give up. So, I had to think in another direction, I still like the thought about a flower, so I continued working with that thought. I had recently colored a mandala and I got an idea to combine a flower with a mandala, making the throbber more like a piece of art. So that is what I did, as you can see in my RunMe. With my throbber I want people to think of a piece of art instead of just a boring throbber. Instead of people waiting, looking at a plain throbber and being annoyed because it takes time loading. Instead, I want people not noticing the time it takes to load the page, because they are intrigued by my throbber. I think it kind of hypnotize people a bit, and hopefully they forget the time.

## What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?

I have used the syntax translate, to move the origin to the center of the canvas. Then I used the for-loop to create more that on rectangle. I also used the syntax rotate, so the rectangles move and therefor I added the syntax scale to make the rectangles 5% smaller every time the loop creates a new rectangle. I also used the push and pop, to determinate what should move, and what not. To decide how fast my rotation should be I put angle in a container, so I could decide the speed.

## Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transac-tion, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?

The first throbber that comes into my mind is when you enter Instagram and you have to load the new post, by trolling your finger down the screen. It is round and consist of many lines that makes a circle. It. Is the same throbber facebook uses. Snapchat is different, the throbber is a ghost, the same ghost that is in their logo. The snapchat throbber is meant to be a fun feature, that gets longer the more you pull down the screen. I think both the facebook and instagram throbber communicates that you are waiting, but the waiting is long, and I think a lot of people gets annoyed by waiting. The throbbes is also used when you are on an internet page, I think that it is especially on webpages that the throbber indicates an long waiting time, which as I wrote earlier on is annoying for me. That’s what I want to change with my throbber. The feeling people should get watching it, is not being bored and annoyed, but being dragged into a kind of hypnoses, where you forget the loading time. I think snapchat kind of does the same thing, with the feeling of not thinking about time.
Hans Lammerant talks in his text “How human and machines negotiate experience of time” about emotions being dependent of human expectations linked to the experience of time in the presence. (Lammerant, p. 89.) This is what I want to point out with my throbber, that the feelings you have about my throbber gives you another experience of time, because you kind of get hypnotized.


## Throbber GIF:
![The program show´s a Throbber](Minix3/giphy.gif "preview")


## Reference: 
Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
