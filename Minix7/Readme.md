# MiniX7 

## Link til koden: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix7/sketch.js


## Link til program:
https://nanna12.gitlab.io/aestetisk-programmering/Minix7/



## Which MiniX have you reworked?

I choose to rework my minix1. I chose it because I really liked it, but also because I thought it would be a good way of looking at my code in a new way and adding something new. When I saw my minix1 I instantly had some thoughts   about thing to change and add, that also made my decision eas-ier. At first, I had a look at my minix4, the capture all, mostly because it is the minix, which I like the least. I just didn´t have any ideas to change it, that is also way I chose to look at some of my others minix and found the first one. 

## What have you changed and why?

The first thing I changed is the place in my code, where I had a lot of rectangles, just copypaste after each other. I change it so instead of having a lot of rectangles in the code, I made a While-loop with a specific distance. I therefore deleted a lot of lines, and made the code look better in the order kind of way. I also changed the position of the rectangles and the frog. I did that because I wanted to add a car, and I thought it would look a bit more realistic if the frog walked from the top of the canvas to the button. Besides that I thought I would look an bit more like an crossover, the fact that I add an car, it had to look like an realistic crossover. 
 This is the only things I changed in the original code. The next thing I did was to add things. I would change the color of the crossover to make it look at bit more realistic. Besides that, I added a car and a traffic light. Or more a traffic light for the pedestrian/frog. I added the traffic light because I wanted the frog to walk, when the light turned green and the car to move when the light was red, and the frog/pedestrian had a red light. I therefore used if-statements with booleans and the syntax mouse-pressed, to make the light switch. I also used this to make the car and frog move at an certain time, determent by the traffic light. 

## What have you learnt in this mini X? How did you incorporate/advance/interprete the con-cepts in your ReadMe/RunMe (the relation to the assigned readings)?

I have a thing for things moving, as you can see if you look through my minix, but I also have diffi-culties understanding if-statements more specifically when to use else, else if and so on. I feel like using a While-loop instead of a for-loop in this minix made me understand the difference between these two. I don´t think I have learnt something new in this minix, I got a better understanding of the difference of a while-loop and for-loop and if-statements.  

## What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?

If you ask me, I think there is an big relation between aesthetic programming and digital culture, I feel like that thinking about making aesthetic programming as an artwork or anything else is determent by the digital culture. An example of this is when you have to make an emoji, you have to make sure you don´t insult people, with their color, culture and so on. Something I gave some thought in making my minx2. I just when in another direction, I wanted my emojis to make awareness of green energy, and didn´t have that many throughs about it if insulted anyone. 
I feel like aesthetic programming or just coding in itself can be a way of expressing yourself and your opinions about the culture you live in or themes around the world. You can use the coding as a way of showing your critical opinion or showing that you care about specific cultural, political, economic themes for example the gender question. You can relate this to inclusive design, designing for a specific group of people.
So, my work this week I would call creative coding, because it consists of shapes, colors and move-ment, just like my first minix did. If I think of all my minx´s I would say the aesthetic programming comes alive by the way I am thinking about my minx´s as an artwork, a cultural aspect or in an critical way of thinking. 


## GIF:
![The program show´s a frog and a car](Minix7/giphy.gif "preview")

## Reference:
Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24
