let y = 0;
/* fy means the two ellipses that are the leg*/
let fy = 0;
let rec = 0
let carx = 0
let redlight = "red"
let greenlight = "grey"
let lightisred = true


function setup(){
  createCanvas(510,480);
  background(52,59,64)

}

  function draw(){
  background(52,59,64)
    /*I make more rect with same size and color.
    i make a while loop. With a 'while loop', the code inside of the loop body
    (between the curly braces) is run repeatedly until the test condition
    (inside of the parenthesis) evaluates to false*/
var rec=0;
while(rec<height){
fill(color(253,254,254))
  rect(215,rec,80,40);
  rec=rec+60; /* this means that the distance between the rect is 60*/
}

  /* if y is bigger then 480, then it will be placed outside my canvas. Thats
  why i set the y-value to 0, så that the circle goes from 510-0 kontinuerligt*/
  if (y > 480) {
    y = 0;
    }
    /* I add tree ellipses. One of them is green, also the bigges. The
    two others is red and smallere.*/
      fill(255,0, 0);
       ellipse(240,fy,10,10);
       fy = y + 15
       fill(255,0, 0);
        ellipse(270,fy,10,10);
        fy = y + 15

/* the big green ellipse*/
        fill(0, 255, 0);
          ellipse(255,y,40,40);
/* making the frog move when the light is green and not red*/
if(lightisred===false){
        y = y + 0.8;
          /* i put the big green ellipse y-value to 0,8, which means That
          the ellipse moves o,8 pixel all the time*/
}
    /* the car*/
  fill(29,50,200);
	rect(carx,198,110,50,20);
	fill(50);
	ellipse(carx,250,40,40);
	ellipse(carx + 110,250,40,40);

/* making the car move and speed of the car and also making it move, when the
light is red*/
if(lightisred===true){
carx=carx+2.2;
}
/* making sure the car gets back into the canvas*/
if (carx>510) {
carx=0;
}

  /* the traficlight*/
   fill(100);
   rect(400,5,60,100);

   fill(redlight);
   ellipse(430,30, 40, 40);

   fill(greenlight);
   ellipse(430,80,40,40);


/* i will add some text to the trafic light*/
fill(color(244,96,63));
textSize(20);
text("Pres the trafic light and see what happens",20,55);
    }
/* now i want to make the car move, when the traficlight is red, and the frog
move, when the traficlight is green*/

  function mousePressed(){
    lightisred=!lightisred /* i use the variable !, that says when the boolean
    is true it turns false and the other way around*/
    if(lightisred===true){
      greenlight="grey"
      redlight="red"

    }
    else if (lightisred===false){
      redlight="grey"
      greenlight="green"
  }


}
