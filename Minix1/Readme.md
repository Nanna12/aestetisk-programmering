## Link til koden: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix1/sketch.js

## Link til program:
https://nanna12.gitlab.io/aestetisk-programmering/Minix1/

## What have i made
For min first mini-exercise, I explored the p5.js motions and color references. Doing so i created a Runme showing a round figure crossing the pedestrian crossing, seen from above. I got the idea after making one rectangle, and then another one, I started thinking it looked like a pedestrian crossing. So i changed the colors of the background and the tiles. A pedestrian crossing needs a person or figure crossing. So i played with the circle’s placement in relation to each other, so it looked like the figure had two feet. I think it look like a frog, but it’s kind of also looks like a person seen from above, with a bigger stomach.  
If I have to say so, I think I am really impressed in the fact that I just made this, my first ever coding and running program.

## Feelings about my first coding experience
At first i was very nervous, having butterflies in my stomach, I was pretty afraid to say that at least. I am not very creative, so i just though this is going to be very hard. When I got home from the first class, I took my computer out and started looking at p5 js´s reference page. I was inspired by Jacob Wang´s first Mini x1. He worked with color and motion, and I through that looked interesting so I decided I wanted to take a look at color and motion myself. I started with the colors, like background and color fillings of figures. Furthermore, I looked at movement, i looked at the reference; "Describe" and decided i would like that to be a part of my mini x1.
After making my first code I felt a kind of released feeling, I made something I was pleased with. It kind of gave me hope for the course, hope that I could complete the course with a satisfaction and a feeling that I did what I could. Now we will see how it goes throughout the semester.     

## The writing of the code
Writing the code for the background and the fillings of the rectangles was easy for me, it made sense after learning about the RGB and how the colors are made. The setup of the program also made more and more sense, why some syntax had to be in the setup function, and some in the draw function. I think the order of the syntax/program is something I have to try looking a bit more at it, to fully un-derstand it.  
The hard part was understanding the movement for the circle, the for-loop but after changing some of the numbers I started noticing what the numbers did to the circle. Somehow, I started understanding why the code had to look like that, but it is still something I find difficult. I really like that I made something move, so I think I will use the for-loop some other time, also the get a better understanding of how it works.  

## Future feeling about coding and aesthetic programming
After this first mini x, I am kind of intrigued and want to learn more. I have already played a little more with some of the other references. So right now, i am very confident, that I have a good chance of learning about aesthetic programming, and how to do coding. I am looking forward to learn to code with an aesthetic aspect, see if I have trouble creating something that produces an critical aspect upon computer science, art and culture.  
As Annette vee talks about in her text that coding is for everyone, I feel like that fits kind of perfect on me, because I have no experience with coding, but after watching some YouTube video´s, hearing Winnie talk and play with it myself I have created something, which I didn´t I would be so impressed with. So yes, I think Annette vee statement about everyone can code is true. 


### Green frog GIF:
![The program show´s a green frog](Minix1/giphy.gif "preview")

### reference:

https://gitlab.com/jakobwangau/aesthetic-programming/-/blob/master/miniX1/readme.md

Video from "The coding train" 1.4 Color.
https://www.youtube.com/watch?v=riiJTF5-N7c&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=5

Video from "The coding train" 4.1 While and for loop.
https://www.youtube.com/watch?v=cnRD9o6odjk&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=18

Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24

Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on BrightSpace under the Literature folder)
