function setup(){
  createCanvas(510,480);
  /* The background is transparent but also green */
  background('rgba(0,255,0,0.25)');}
  /* i make a varible, right now x=0, i want to change that later in my code to make the circle move*/
let x = 0;
/* fx means the two other circles/ellipses/feet*/
let fx=0;

  function draw(){
    /*I make more rectangles, with the same room between them and same color*/
fill(color(0,200,255))
  rect(20,190,40,80);
  fill(color(0,200,255))
  rect(80,190,40,80);
  fill(color(0,200,255))
  rect(140,190,40,80);
  fill(color(0,200,255))
  rect(200,190,40,80);
  fill(color(0,200,255))
  rect(260,190,40,80);
  fill(color(0,200,255))
  rect(320,190,40,80);
  fill(color(0,200,255))
  rect(380,190,40,80);
  fill(color(0,200,255))
  rect(440,190,40,80);
  fill(color(0,200,255))
  background('rgba(0,255,0,0.25)');
  /*If x is bigger than 510, it is outside my canvas. Thats way i out the x-value to be 0, så the circle goes from 510-0 kontinuerligt*/
  if (x > 510) {
    x = 0;
    }
    /* i add three ellipses. One of them is green, the biggest. The two others function as feets, therefore they are smaller and red.*/
      fill(255,0, 0);
       ellipse(fx, 215, 10, 10);
       fx = x + 15
       fill(255,0, 0);
        ellipse(fx, 245, 10, 10);
        fx = x + 15
        fill(0, 255, 0);
          ellipse(x, 230, 40, 40);
          x = x + 0.8;
          /* I put one of the circles x-value to 0,8, which means the circle moves 0,8 pixel all the time.*/
    }
