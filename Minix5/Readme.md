# MiniX5 

## Link til koden: 
https://gitlab.com/Nanna12/aestetisk-programmering/-/blob/main/Minix5/sketch.js

## Link til program:
https://nanna12.gitlab.io/aestetisk-programmering/Minix5/


# What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?

I have made two rules in my code. The first one is that I made three circles, and the circles had to have a specific order. Meaning that the first circle is the round one, and then the other is showed after. I also made a forloop so that it continues to shift between the three circles. The second rule I made is that the three circles had to shift between 8 colors. As I did with the circles, I also made a color counter, so that the circles goes thought the 8 colors and then start over again. That way I made an array with the colors. As you can see in my code, I used the HEX value for the colors, because I had trouble writing the colors as RGB in the array. I started writing the colors as words, but I would like the colors to be a bit more laid back. Meaning that the colors lightness needed to be a bit lower. 
I wanted to have the background change when you click on the canvas, with your mouse. It works, but when I click the canvas it’s kind of restart the circles again. So, when you click the canvas it changes background color and resets the canvas, so new circles occur. After seeing it more and more I kind of liked it better. 

# So how does my program perform over time and how do the rules produce emergent behavior?

I have used a forloop to make sure that the circles continue to arise in my canvas. So over time more and more circles will occur, and they will be on top of each other and in the end the hole canvas will be filled with three different sizes of circles in different colors. The rules in my program makes it more predictable, because I have the rule decide what color and what circle is to be made. But the fact that I have used the x and y coordinate as random, makes is unpredictable to know where on the canvas the circles will appear. 

# What role do rules and processes have in your work?

It decides how my RunMe/program is going to look. More specific it tells the program that it should shift between three circles and the circles have to change between the colors in the array. I have explained this an bit more I the previous section.  



# Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?

I understand auto-generator as something that is subject to rules, which means that the program is following a pattern. This is used by designers to make an artwork that is conceptual and defined by rules. If you look at my program is it also defined by two rules and by running the program, the rules determent what happens on the canvas/screen. That makes it an artwork.
My program has a high level of control, since I have made rules that tells the program exactly what to do, I have chosen the three circles that the program shifts between and I have chosen the colors. The only things the program decides for itself is the background color, which I have set to random, and there the x and y coordinates is places on the canvas. 
”Computers are commonly thought to order the world, to sift through reams of data and then model possible outcomes, possible futures, providing certain—and deterministic—answers. Yet a function to generate random numbers was present in the first Dartmouth BASIC” ( Nick Montfort et al, page 131).
If we look further down, you could argue that it is not completely the program that decides which colors it chooses for background color, but the colors it chooses from it predecided, because P5.js already have chosen which colors the program have to choose from. 


## GIF:
![The program show´s a button](Minix5/giphy.gif "preview")

## Reference:
Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
