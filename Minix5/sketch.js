var bgColor;
let x=0
let y=0
/* it is colors,but written as HEX, not RGB*/
let colors=['#E3874F','#F7DC6F','#7DCEA0','#AED6F1','#D7BDE2','#7FB3D5','#EC7063','pink'];
let colcounter=1;

function setup(){
  createCanvas(windowWidth,windowHeight);
  /* i set the background color to random*/
  bgColor= color(random(255),random(255),random(255));
  background(bgColor);
  frameRate(20);
}

function draw(){
  /* i am gonna make three circles, that is going to be determinated by my rules*/
  /* it rule time!*/
  /* the first rule is that the circles have to change between 7 colors*/
  fill(color(colors[colcounter]));
  colcounter++
  if(colcounter===8)
  colcounter=0

/* This forloop says that it should draw three different circles*/
for (var i=0; i < 4; i++){
    /* the second rule, is that the diffrent circles appears after one another*/
  if(i===1){
    x=random(windowWidth);
    y=random(windowHeight);
    ellipse(x,y,60,60);

}
else if(i===2){
  x=random(windowWidth);
  y=random(windowHeight);
  ellipse(x,y,20,80);

}
else if(i===3){
  x=random(windowWidth);
  y=random(windowHeight);
  ellipse(x,y,60,30);

}
}
}
function mousePressed(){
bgColor=color(random(255),random(255),random(255));
    background(bgColor);
}
